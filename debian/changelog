liblingua-en-namecase-perl (1.21-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + liblingua-en-namecase-perl: Add Multi-Arch:
    foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 22 Oct 2022 10:42:29 +0100

liblingua-en-namecase-perl (1.21-1) unstable; urgency=medium

  * Take over for the Debian Perl Group on request of the MIA team.
  * debian/control: Added: Vcs-Git field (source stanza); Vcs-Browser
    field (source stanza); ${misc:Depends} to Depends: field. Changed:
    Homepage field changed to metacpan.org URL; Maintainer set to Debian
    Perl Group <pkg-perl-maintainers@lists.alioth.debian.org> (was: C.
    Chad Wallace <cwallace@lodgingcompany.com>).
    Fixes "invalid maintainer address" (Closes: #979502)
  * debian/watch: use uscan version 4.
  * debian/watch: use metacpan-based URL.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 4.5.1.
  * Set Rules-Requires-Root: no.
  * Drop unneeded version constraints from (build) dependencies.
  * Switch to source format "3.0 (quilt)".
  * Bump debhelper-compat to 13.
    Fixes "Removal of obsolete debhelper compat 5 and 6 in bookworm"
    (Closes: #965658)
  * debian/rules: use dh(1). And stop installing README.
  * debian/copyright: use Copyright-Format 1.0.
  * Add /me to Uploaders.
  * Mention module name in long description.

  * Import upstream version 1.21.
  * Update upstream maintainer, copyright holders, and license.
  * Add debian/upstream/metadata.
  * Add build-dependency on libtest-most-perl.

 -- gregor herrmann <gregoa@debian.org>  Sun, 17 Jan 2021 21:53:03 +0100

liblingua-en-namecase-perl (1.15-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Wed, 06 Jan 2021 19:43:46 +0100

liblingua-en-namecase-perl (1.15-1) unstable; urgency=low

  * New upstream version. (Closes: #465884)
  * Rebuilt the package using the newest dh-make-perl. (Closes: #467882)
  * Added a watch file for cpan.

 -- C. Chad Wallace <cwallace@lodgingcompany.com>  Thu, 28 Feb 2008 18:20:07 -0800

liblingua-en-namecase-perl (1.13-1) unstable; urgency=low

  * Initial Release.

 -- C. Chad Wallace <cwallace@thelodgingco.com>  Fri, 21 Jan 2005 15:08:13 -0800
